from dbconn import Connection
from datetime import datetime


class Model:

    conn = Connection().sqlconnect

    """класс для работы с запросами в базу данных
    1. connectdb.cursor  ---  __init__
    2. cursor.execute()   ---   def executedb
    3. connectdb.commit()  ---


    Методы:
        печатуют в терминал
        требуют ввод данных
        открывают соединение через менеджер контекста
        передают введенные данные в sql запрос
        делаю commit()

    """


    printwords = [
        "enter title : ",
        "enter author : ",
        "enter book id : ",
        "enter price : ",
        "enter status : ",
        "enter new price : ",
        "enter new status : ",
        "enter search word : "
    ]

    def commitfunc(self, sqlreq, value):  # 
        # try:
        with self.conn.cursor() as cursor:
            cursor.execute(sqlreq, value)
            self.conn.commit()
        return print("successful")
        # except:
        #     return print("cursor error")
    
    def fetchfunc(self, sqlreq, value):
        try:
            with self.conn.cursor() as cursor:
                cursor.execute(sqlreq, value)
                result = cursor.fetchone()
            if result != None:
                return result
            else: 
                return 'Not found'
        except:
            return print("fetch error")

    def addtable(self):
        s = """create table if not exists Book (
        id int primary key auto_increment,
        title varchar(100) unique not null,
        author varchar(100) not null,
        price decimal(10, 2) unsigned not null,
        status boolean default True,
        published_at date not null)"""
        with self.conn.cursor() as cursor:
            cursor.execute(s)
            self.conn.commit()
        return print(" successful")

        
    def addbook(self, pw1, pw2, pw4):
        title = input(pw1)
        author = input(pw2)
        price = input(pw4)
        now = datetime.now()
        date = now.strftime('%Y-%m-%d')
        s = "insert into Book(title, author, price, published_at) values(%s, %s, %s, %s)"
        a = (title, author, price, date)
        return self.commitfunc(s, a)

    
    def showallbook(self):
        s = "select * from Book;"
        try:
            with self.conn.cursor() as cursor:
                cursor.execute(s)
                result = cursor.fetchall()
            return print(result)
        except:
            return print('error')
    
    def showbook(self, pw):
        id = input(pw)
        s = "select * from Book where id=%s"
        return print(self.fetchfunc(s, id))

    
    def changedata(self, pw2, pw):
        if pw == self.printwords[5]:
            id = input(pw2)
            s = "select * from Book where id = %s;"
            req = self.fetchfunc(s, id)
            if req != None:
                newprice = input(pw)
                a = "update Book set price=%s where id=%s;"
                arg = (newprice, id)
                return self.commitfunc(a, arg)
            else:
                return print('not found')
        elif pw == self.printwords[6]:
            id = input(pw2)
            s = "select * from Book where id = %s;"
            req = self.fetchfunc(s, id)
            if req != None:
                newstatus = input(pw)
                str = "update Book set status=%s where id=%s;"
                arg = (newstatus, id)
                return self.commitfunc(a, arg)
            else:
                print('error change data')

    
    def removebook(self, pw):  # требует ввод id
        id = int(input(pw))
        s = "select * from Book where id = %s;"
        req = self.fetchfunc(s, id)
        if req != None:
            a = input('Are you sure you want to delete the record number ?\nEnter yes : ')
            if a == 'yes':
                s2 = "delete from Book where id=%s"
                return self.commitfunc(s2, id)
            else:
                print('You canceled the deletion!')
        else:
            return print('not found')

    
    def search(self, pw):  # требует ввод подстроки
        inp = input(pw)
        substring = f'%{inp}%'
        # s = "select * from Book where title like %s;"
        with self.conn.cursor() as cursor:
            sql=f"""select * from Book where title like "%{inp}%" or author like "%{inp}%";"""
            cursor.execute(sql)
        return print(cursor.fetchall())
