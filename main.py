from models import Model
from sys import argv


class Commands:

    sqlrequest = ["add table", "add book", "show book", "show all", "change status", "change price", "remove", "search"]

    def inputdata(self):
        argventer = argv
        inputstring = ""
        for i in argventer[1:]:
            inputstring += str(i) + " "
        inpstr = inputstring.lower()
        a = inpstr.strip()
        return a
    
    def command(self):
        inp = self.inputdata()  # вызываем argv
        c = 0
        for key in self.sqlrequest:
            if inp == key:
                break
            c += 1
        callm = self.callmethod(c)
        return callm
    
    def callmethod(self, c):
        model = Model()
        x = Model.printwords
        if c == 0:
            model.addtable()
        elif c == 1:
            model.addbook(x[0], x[1], x[3])
        elif c == 2:
            model.showbook(x[2])
        elif c == 3:
            model.showallbook()
        elif c == 4:
            model.changedata(x[2], x[6])
        elif c == 5:
            model.changedata(x[2], x[5])
        elif c == 6:
            model.removebook(x[2])
        elif c == 7:
            model.search(x[7])
        else:
            print('input error.\n enter to again:\n add table,\n add book,\n show book,\n show all,\n change status,\n change price,\n remove,\n search')
        return None

conn = Commands()

if __name__ == "__main__":
    conn.command()
